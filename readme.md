# Lazy loading

## What is it

Lightweight (2.1kb minified) images lazy loading polyfill for browsers that don't support the native attribute. Supports image and picture tags,
supports dynamic content (e.g. ajax). Based on Intersection and Mutation Observers.
If a browser supports native lazy loading then the script just will put a real source from the data attribute to the
target. Otherwise, will implement lazy loading via IntersectionObserver

## Installation

```
yarn add @lightsource/lazy-loading
```

OR

```
npm install @lightsource/lazy-loading
```

## Example of usage

#### Html structure

An image

```
<img class='lazy-loading' <!-- setup a class, the class name can be changed through settings -->
     src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" <!-- default stub -->
     data-src="example.com/picture" <!-- real source -->
     alt="Some description"
     data-srcset="example.com/picture 500w, example.com/picture2 1000w" <!-- optional -->
     loading="lazy">
```

OR

A picture

```
<picture>

 <source data-srcset="example.com/picture" <!-- real source -->
         type="image/jpg"> <!-- image's mime type -->

<img class='lazy-loading' <!-- setup a class, the class name can be changed through settings -->
     src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" <!-- default stub -->
     data-src="example.com/picture" <!-- real source -->
     alt="Some description"
     loading="lazy">

</picture>
```

#### JavaScript/Typescript

You can import it like an ordinary npm package, but for the best performance I recommend another way - at the end of a
body tag add belows code :  
(only at the end images are already loaded, also don't use external script to avoid an extra request)

```
<script>
(function () {
... TODO copy code from lazyLoading.min.js
new Images({
 yOffsetToLoading: 500,
 toLoadClass: 'lazy-loading',
 loadedClass: 'lazy-loading--loaded',
 loadedEvent: 'lazy-loading_loaded',
}) // all arguments are optional
}())
</script>
```
