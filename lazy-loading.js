class LazyLoading {
    constructor(settings) {
        this.settings = settings;
        this.intersectionObserver = null;
        this.options = {
            root: null,
            rootMargin: settings.yOffsetToLoading + 'px' + ' 0px',
            threshold: 0.01,
        };
        this.isBrowserSupportsNative = 'loading' in HTMLImageElement.prototype;
        this.init();
    }
    static onImageLoaded(settings, event) {
        let image = event.target;
        image.classList.remove(settings.loadingClass);
        image.classList.add(settings.loadedClass);
        image.dispatchEvent(new Event(settings.loadedEvent));
    }
    load(lazyImage) {
        let src = lazyImage.dataset['src'];
        let srcSet = lazyImage.dataset['srcset'];
        if (!src) {
            console.log('Element\'s data for lazyLoading is missing', lazyImage);
            return;
        }
        lazyImage.removeAttribute('data-src');
        if (srcSet) {
            lazyImage.removeAttribute('data-srcset');
        }
        lazyImage.classList.add(this.settings.loadingClass);
        // event listener before any changes
        lazyImage.addEventListener('load', LazyLoading.onImageLoaded.bind(null, this.settings));
        // update picture sources
        if (lazyImage.parentElement &&
            'PICTURE' === lazyImage.parentElement.tagName) {
            lazyImage.parentElement.querySelectorAll('source').forEach((source) => {
                let sourceSrcSet = source.dataset['srcset'];
                if (!sourceSrcSet) {
                    console.log('Element\'s data for lazyLoading is missing', source);
                    return;
                }
                source.setAttribute('srcset', sourceSrcSet);
                source.removeAttribute('data-srcset');
            });
        }
        // update the image
        if (srcSet) {
            lazyImage.setAttribute('srcset', srcSet);
        }
        lazyImage.setAttribute('src', src);
    }
    init() {
        if (this.isBrowserSupportsNative) {
            return;
        }
        if (!window.hasOwnProperty('IntersectionObserver')) {
            console.log('LazyLoading failed, IntersectionObserver doesn\'t supported by the browser');
            return;
        }
        this.intersectionObserver = new IntersectionObserver(this.onIntersectionCallback.bind(this), this.options);
    }
    add(lazyImage) {
        if (this.isBrowserSupportsNative ||
            !this.intersectionObserver) {
            this.load(lazyImage);
            return;
        }
        this.intersectionObserver.observe(lazyImage);
    }
    onIntersectionCallback(entries, observer) {
        entries.forEach(entry => {
            if (!entry.isIntersecting) {
                return;
            }
            let target = entry.target;
            this.load(target);
            observer.unobserve(target);
        });
    }
}
class Images {
    constructor(settings = {}) {
        settings = Object.assign({
            yOffsetToLoading: 500,
            toLoadClass: 'lazy-loading',
            loadingClass: 'lazy-loading--loading',
            loadedClass: 'lazy-loading--loaded',
            loadedEvent: 'lazy-loading_loaded',
        }, settings);
        this.settings = settings;
        this.lazyLoading = new LazyLoading(settings);
        this.mutationObserver = null;
        this.processImages(document.body);
        this.createObserver();
    }
    createObserver() {
        if (!window.hasOwnProperty('MutationObserver')) {
            console.log('LazyLoaded failed. MutationObserver doesn\'t supported');
            return '';
        }
        this.mutationObserver = new MutationObserver(this.onMutationCallback.bind(this));
        this.mutationObserver.observe(document.body, {
            childList: true,
            subtree: true,
        });
    }
    processImages(target) {
        if (Node.ELEMENT_NODE !== target.nodeType) {
            return;
        }
        if (target.classList.contains(this.settings.toLoadClass)) {
            // for sliders and others that create clones
            if (!target.classList.contains(this.settings.loadingClass) &&
                !target.classList.contains(this.settings.loadedClass)) {
                this.lazyLoading.add(target);
            }
        }
        target.querySelectorAll('.' + this.settings.toLoadClass).forEach((image) => {
            // for sliders and others that create clones
            if (!image.classList.contains(this.settings.loadingClass) &&
                !image.classList.contains(this.settings.loadedClass)) {
                this.lazyLoading.add(image);
            }
        });
    }
    onMutationCallback(records, observer) {
        records.forEach((record, index) => {
            record.addedNodes.forEach((element, index2) => {
                this.processImages(element);
            });
        });
    }
}
